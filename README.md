# GitLab CLI Tools

A small set of tools for using the GitLab API from the command line.

## `keyset-pager`: GitLab Keyset Pager API Client

This is a small utility for querying the GitLab API.

It returns list results in NDJSON format, suitable for use with tools such as `jq`.

It will return all pages of results, and uses [keyset pagenation](https://docs.gitlab.com/ee/api/#keyset-based-pagination).

It is built on top of `github.com/xanzy/go-gitlab`, and as such supports retry and handles rate limits.

### `keyset-pager` Usage

Pass the API URL `keyset-pager` to obtain all results in NDJSON format:

```console
$ keyset-pager "https://gitlab.com/api/v4/projects?owned=true"
# Newline delimited JSON results
# ...
```

**Note:** the token set in `GITLAB_TOKEN` will be used for the request, if set.

To extract a particular field from the results, use `jq` as follows:

```console
$ keyset-pager 'https://gitlab.com/api/v4/projects?owned=true&visibility=public'|jq -r .web_url
https://gitlab.com/gitlab-com/gl-infra/infrastructure
https://gitlab.com/gitlab-com/gl-infra/toolbelt
https://gitlab.com/gitlab-com/gl-infra/prometheus-git-exporter
https://gitlab.com/gitlab-com/gl-infra/oncall-robot-assistant
```

Details of the parameters that can be used are documented in the [GitLab API Documentation](https://docs.gitlab.com/ee/api/projects.html#list-all-projects).

## `custom-attrib`: Set and remove custom attributes on GitLab Projects, Users and Groups

This tool allows administrators to set and remove [custom attributes](https://docs.gitlab.com/ee/api/custom_attributes.html) from GitLab projects, users and groups.

This requires an administrator token. The token should be set in the `GITLAB_TOKEN` variable.

**Note**: the tool does not support retrieval of tokens. This can be done with `keyset-pager` by passing in the `with_custom_attributes=true` parameter on the URL.

### `custom-attrib` Usage

### Setting and retrieving custom attributes on multiple projects

```console
# Populate a file containing project ids and project paths for projects for which you want to set custom attributes
$ echo "h5bp/html5-boilerplate" > projects.txt # # Handles full project paths
$ echo "7" >> projects.txt # Supports IDs too
# Update the projects with X=Y for all projects in file `projects.txt`
$ custom-attrib set --resource projects --key X --value Y projects.txt
# List all projects with the custom attribute of X=Y
$ keyset-pager 'https://gitlab.com/api/v4/projects?custom_attributes[X]=Y&with_custom_attributes=true'|jq '[.web_url, .custom_attributes]'
[
  "https://gitlab.com/h5bp/html5-boilerplate",
  [
    {
      "key": "X",
      "value": "Y"
    }
  ]
]
```

### Removing a custom attribute from multiple projects

```console
# Populate a file containing project ids and project paths for projects for which you want to set custom attributes
$ echo "h5bp/html5-boilerplate" > projects.txt # Handles full project paths
$ echo "7" >> projects.txt # Supports IDs too
# Remove the Custom Attribute X for all projects in file `projects.txt`
$ custom-attrib remove --resource projects --key X projects.txt
```

### Passing resources via `stdin`

Resource identifiers can be passed via `stdin` by specifying `-` as the input file.

This example sets the `support_plan` attribute to `gold` for the `h5bp` group.

```console
$ echo "h5bp" | custom-attrib set --resource groups --key support_plan --value gold -
# List all groups with a gold `support_plan` attribute
$ keyset-pager 'https://gitlab.com/api/v4/groups?custom_attributes[suppport_plan]=gold'
```

### Setting a custom attribute for a user

**Note**: the GitLab Users API does not support accessing user resources by username, therefore all IDs must by integer values.

```console
$ echo "62" | custom-attrib set --resource users --key staff --value true -
# List all users with a `staff` attribute
$ keyset-pager 'https://gitlab.com/api/v4/users?custom_attributes[staff]=true'
```
