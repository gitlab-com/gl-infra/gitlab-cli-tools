package main

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/alecthomas/kong"
	"github.com/xanzy/go-gitlab"
)

func forEach(file *os.File, processor func(string) error) error {
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		err := processor(scanner.Text())
		if err != nil {
			return err
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func setCustomAttribute(client *gitlab.Client, resource string, id string, c gitlab.CustomAttribute) error {
	u := fmt.Sprintf("%s/%s/custom_attributes/%s", resource, id, c.Key)
	req, err := client.NewRequest("PUT", u, c, nil)
	if err != nil {
		return err
	}

	ca := new(gitlab.CustomAttribute)
	_, err = client.Do(req, ca)
	return err
}

func deleteCustomAttribute(client *gitlab.Client, resource string, id string, key string) error {
	u := fmt.Sprintf("%s/%s/custom_attributes/%s", resource, id, key)
	req, err := client.NewRequest("DELETE", u, nil, nil)
	if err != nil {
		return err
	}
	_, err = client.Do(req, nil)
	return err
}

type setCmd struct {
	Resource string `required name:"resource" help:"Entity" enum:"projects,groups,users"`
	Key      string `required name:"key" help:"Custom attribute key"`
	Value    string `required name:"value" help:"Custom attribute value"`

	Input *os.File `arg required name:"input" help:"File to read project identifiers from"`
}

func (r *setCmd) Run(client *gitlab.Client) error {
	c := gitlab.CustomAttribute{Key: r.Key, Value: r.Value}

	return forEach(r.Input, func(line string) error {
		line = strings.ReplaceAll(line, "/", "%2f")
		return setCustomAttribute(client, r.Resource, line, c)
	})
}

type removeCmd struct {
	Resource string   `required name:"resource" help:"Entity" enum:"projects,groups,users"`
	Key      string   `required name:"key" help:"Custom attribute key"`
	Input    *os.File `arg name:"input" help:"File to read project identifiers from"`
}

func (r *removeCmd) Run(client *gitlab.Client) error {
	return forEach(r.Input, func(line string) error {
		line = strings.ReplaceAll(line, "/", "%2f")
		return deleteCustomAttribute(client, r.Resource, line, r.Key)
	})
}

var cli struct {
	APIBase url.URL   `name:"api-base" default:"https://gitlab.com/api/v4"`
	Set     setCmd    `cmd help:"Set custom attribute"`
	Remove  removeCmd `cmd help:"Remove custom attribute"`
}

func main() {
	ctx := kong.Parse(&cli)

	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"), gitlab.WithBaseURL(cli.APIBase.String()))
	ctx.FatalIfErrorf(err)

	// Call the Run() method of the selected parsed command.
	err = ctx.Run(git)
	ctx.FatalIfErrorf(err)
}
