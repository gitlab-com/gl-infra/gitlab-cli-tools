package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/alecthomas/kong"
	"github.com/google/go-querystring/query"
	"github.com/peterhellberg/link"
	"github.com/pkg/errors"
	"github.com/xanzy/go-gitlab"
)

// cli defines command-line flags for kong
var cli struct {
	URL url.URL `arg name:"url" help:"URL to request"`
}

// queryValues is a special (hacked) coder for
// passing url.Values into GitLab-Go
type queryValues struct {
	Values url.Values `url:"v"`
}

func (c *queryValues) EncodeValues(key string, v *url.Values) error {
	v.Add("pagination", "keyset")
	v.Add("per_page", "50")
	v.Add("order_by", "id")
	v.Add("sort", "asc")

	for k, val := range c.Values {
		if k == "pagination" || k == "per_page" || k == "order_by" || k == "sort" {
			continue
		}
		for _, item := range val {
			v.Add(k, item)
		}
	}

	return nil
}

var _ query.Encoder = &queryValues{}

// options is for passing options to GitLab-Go
type options struct {
	Values *queryValues `url:"values"`
}

// getURLComponents breaks a URL up into components
func getURLComponents(u url.URL) (base string, apiRelativePath string, err error) {
	segments := strings.Split(u.Path, "/")
	if len(segments) < 3 {
		return "", "", errors.Errorf("Invalid URL: %v", u)
	}

	ru, err := url.Parse("/" + segments[1] + "/" + segments[2])
	if err != nil {
		return "", "", err
	}

	baseURL := u.ResolveReference(ru)
	return baseURL.String(), strings.Join(segments[3:], "/"), nil
}

// requestJSON will request a URL, return an array of JSON objects, the next keyset page and an error
func requestJSON(url url.URL, git *gitlab.Client) ([]map[string]interface{}, *url.URL, error) {
	_, apiRelativePath, err := getURLComponents(url)
	if err != nil {
		return nil, nil, err
	}

	req, err := git.NewRequest("GET", apiRelativePath, &options{&queryValues{url.Query()}}, nil)
	if err != nil {
		return nil, nil, err
	}

	var b bytes.Buffer
	writer := bufio.NewWriter(&b)

	resp, err := git.Do(req, writer)
	if err != nil {
		return nil, nil, err
	}

	jsonArray := []map[string]interface{}{}
	err = json.Unmarshal(b.Bytes(), &jsonArray)
	if err != nil {
		return nil, nil, err
	}

	links := link.ParseHeader(resp.Header)
	next := links["next"]

	if next != nil && next.URI != "" {
		nextURL, err := url.Parse(next.URI)
		if err != nil {
			return nil, nil, err
		}
		return jsonArray, nextURL, nil
	}

	// No more rows
	return jsonArray, nil, nil
}

// execute is the main wrapper, returning an error
func execute() error {
	baseURL, _, err := getURLComponents(cli.URL)
	if err != nil {
		return err
	}

	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"), gitlab.WithBaseURL(baseURL))
	if err != nil {
		return err
	}

	nextURL := &cli.URL
	for {
		var jsonArray []map[string]interface{}
		jsonArray, nextURL, err = requestJSON(*nextURL, git)

		if err != nil {
			return err
		}

		for _, v := range jsonArray {
			jsonObj, err := json.Marshal(v)
			if err != nil {
				return err
			}
			fmt.Printf("%s\n", jsonObj)
		}

		if nextURL == nil {
			break
		}
	}

	return nil
}

func main() {
	kong.Parse(&cli)

	err := execute()
	if err != nil {
		log.Fatalf("error: %v", err)
	}
}
