module gitlab.com/gitlab-com/gl-infra/gitlab-cli-tools

go 1.13

require (
	github.com/alecthomas/kong v0.2.12
	github.com/google/go-querystring v1.0.0
	github.com/peterhellberg/link v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/xanzy/go-gitlab v0.40.1
)
